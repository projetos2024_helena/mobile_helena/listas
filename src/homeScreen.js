import React from "react";
import {View, Button} from "react-native";
import {useNavigation} from "@react-navigation/native";

const HomeScreen = () => {
    const navigation = useNavigation();
    const handleUsers = () => {
        navigation.navigate("Users");
    }

    return(
        <View>
        <Button title="ver usuarios" onPress={handleUsers}></Button>
        </View>
    )
};
export default HomeScreen;